import requests
import pandas as pd

URL_USER_FRIENDS = 'https://www.livejournal.com/misc/fdata.bml?user='


class LiveJournalService:

    def __init__(self):
        pass

    @staticmethod
    def get_user_friends(username):
        response = requests.get(URL_USER_FRIENDS + username)
        friends_list = list(
            set([user.replace('< ', '').replace('> ', '') for user in response.text.splitlines()[1:]][:-1]))
        return friends_list

    @staticmethod
    def get_user_network(start_user, rounds_number):
        network_df = pd.DataFrame([], columns=['user', 'friends', 'level', 'parent'])
        start_friend_list = LiveJournalService.get_user_friends(start_user)
        network_df = network_df.append(LiveJournalService._create_user_series(start_user,
                                                                              start_friend_list,
                                                                              0), ignore_index=True)
        for i in range(rounds_number):
            round_df = network_df[network_df['level'] == i]
            for index, row in round_df.iterrows():
                user, friends = row['user'], row['friends']
                for friend in friends:
                    print(friend)
                    if not (friend == network_df['user']).any():
                        friend_list = LiveJournalService.get_user_friends(friend)
                        network_df = network_df.append(LiveJournalService._create_user_series(friend,
                                                                                              friend_list,
                                                                                              i + 1,
                                                                                              user),
                                                       ignore_index=True)
        return network_df

    @staticmethod
    def _create_user_series(username, friends_list, level, parent=None):
        return pd.Series({'user': username,
                          'friends': friends_list,
                          'level': level,
                          'parent': parent})
