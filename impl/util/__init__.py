import pprint
import time
import numpy as np
import pandas as pd


def graph_simulation_report(N, param, func, **kwargs):
    start = time.time()
    graph = func(N, param, **kwargs)
    stop = time.time()
    time_ = stop - start
    print(time_)
    nodes_number = len(graph.nodes())
    edges_number = len(graph.edges())
    degrees = graph.get_degrees()
    mean_degree = np.mean(degrees)
    variance_degree = np.var(degrees)
    return {'N': N,
            'param': param,
            'compute_time': time_,
            'nodes_number': nodes_number,
            'edges_number': edges_number,
            'degrees_list': degrees,
            'mean_degree': mean_degree,
            'variance_degree': variance_degree}


def generate_simulation_df(N, param_list, generate_func, **kwargs):
    df = pd.DataFrame([])
    for i in param_list:
        simulation_result = graph_simulation_report(N, i, generate_func, **kwargs)
        df = df.append(pd.Series(simulation_result), ignore_index=True)
    return df


def generate_power_law_df(df, N, m_list):
    df_out = pd.DataFrame([], columns=['x', 'y', 'm'])
    x = np.arange(N)
    for i in range(df.shape[0]):
        y = np.bincount(df['degrees_list'][i]) / N
        degrees = pd.DataFrame(zip(x, y), columns=['x', 'y'])
        degrees['m'] = m_list[i]
        degrees = degrees[degrees['y'] != 0]
        df_out = df_out.append(degrees).reset_index(drop=True)
    df_out['x'] = df_out['x'].apply(lambda x: np.log10(x))
    df_out['y'] = df_out['y'].apply(lambda x: np.log10(x))
    return df_out


def check_agent_time(df_trajectory, query):
    df_query = df_trajectory.query(query)
    return len(df_query) / len(df_trajectory)


def monte_carlo_random_walk(n_trajectories, n_steps, agent, query, random_walk_type='pearson'):
    result_list = np.array([check_agent_time(agent.simulate_random_walk(max_steps=n_steps, type_=random_walk_type),
                                             query) for i in range(n_trajectories)])
    return result_list


def get_node_hitting_times(graph, starting_node):
    time_ = 1
    result_dict = {}
    current_node = starting_node
    while len(result_dict) != len(graph.nodes()) - 1:
        current_node = np.random.choice(graph.get_neighbours(current_node))
        if current_node not in result_dict and current_node != starting_node:
            result_dict[current_node] = time_
        time_ += 1
    df = pd.Series(result_dict).reset_index().rename({'index': 'node', 0: 'time'}, axis=1)
    return df


def monte_carlo_hitting_times(graph, n_sim, starting_node):
    df_res = pd.DataFrame([])
    for i in range(n_sim):
        df_res = df_res.append(get_node_hitting_times(graph, starting_node)).reset_index(drop=True)
    df_out = df_res.groupby(['node']).agg({'time': np.mean}).reset_index()
    return df_out


def find_most_central_nodes(centrality_measure_dict):
    series = pd.Series(centrality_measure_dict)
    max_degree_nodes = list(series[series == series.max()].to_dict().keys())
    return max_degree_nodes


def print_centrality_measure_info(centrality_dict, max_node, title):
    pprint.pprint(title)
    pprint.pprint(centrality_dict)
    pprint.pprint(f'Who is the most central node:')
    pprint.pprint(max_node)
