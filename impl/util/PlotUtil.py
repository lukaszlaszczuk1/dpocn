import os
import time
import networkx as nx
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from typing import Any, Callable, Dict, List, Union
from plotly.subplots import make_subplots
from scipy.stats import binom

from impl.models.Graph import Graph
from impl.models.generators import generate_random_graph, generate_watts_strogatz, generate_barabasi_albert


class PlotUtil:
    def __init__(self):
        pass

    @staticmethod
    def plot_graph_plotly(graph: Union[nx.Graph, Graph], title: str,
                          layout_function: Callable = nx.kamada_kawai_layout,
                          color='node_adjacencies'):
        """ Creates graphical representation of a graph in Plotly.

        Parameters:
            graph (Union[nx.Graph, Graph]): Graph representation in networkx or own implementation consistent with networkx framework.
            title (str): Title of the plot.
            layout_function (Callable): Function for creating the layout of a graph.
            color (Any): How should colors be assigned?

        Returns:
            fig (plotly.graph_objects.Figure): Plot representing given graph.
        """
        if not isinstance(graph, nx.Graph):
            positions = layout_function(graph.nodes())
        else:
            positions = layout_function(graph)
        edge_x, edge_y = PlotUtil._get_edge_positions(graph, positions)
        node_x, node_y = PlotUtil._get_node_positions(graph, positions)
        node_adjacencies, node_text = PlotUtil._get_node_adjacencies(graph)

        edge_trace = go.Scatter(
            x=edge_x, y=edge_y,
            line=dict(width=3.5, color='black'),
            hoverinfo='none',
            mode='lines')

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers+text',
            hoverinfo='text',
            text=list(graph.nodes()),
            textfont=dict(
                family="sans serif",
                size=15,
                color="black"
            ),
            marker=dict(
                showscale=True,
                colorscale='Viridis',
                reversescale=True,
                color=[],
                size=50,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line_width=2))

        node_trace.marker.color = node_adjacencies if color == 'node_adjacencies' else ['blue' if i != color else 'red'
                                                                                        for i in graph.nodes()]

        fig = go.Figure(data=[edge_trace, node_trace],
                        layout=go.Layout(
                            title=title,
                            titlefont_size=22,
                            showlegend=False,
                            hovermode='closest',
                            margin=dict(b=20, l=5, r=5, t=40),
                            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
                        )
        return fig

    @staticmethod
    def generate_graphs(p_list, generator_type, N, width, height, **kwargs):
        title = "Random" if generator_type == "random_graphs" else "Watts-Strogatz" if generator_type == "watts_strogatz" else "Barabasi Albert"
        graph_func = generate_random_graph if generator_type == "random_graphs" else generate_watts_strogatz if generator_type == "watts_strogatz" else generate_barabasi_albert
        fig = make_subplots(rows=len(p_list), cols=1,
                            subplot_titles=[f'{title} graph (N={N}, p={round(p, 2)})' for p in p_list])
        for i, p in enumerate(p_list):
            graph = graph_func(N, p, **kwargs)
            plot = PlotUtil.plot_graph_plotly(graph, f'{title} graph - List 3 (N={N}, p={round(p, 2)})')
            fig.add_trace(plot.to_dict()['data'][0], row=i + 1, col=1)
            fig.add_trace(plot.to_dict()['data'][1], row=i + 1, col=1)

        fig.update_layout(height=height,
                          width=width,
                          showlegend=False)
        fig.show()
        fig.write_html(os.path.join('l3', 'fig', generator_type, 'html', f'{generator_type}_comparison.html'))
        fig.write_image(os.path.join('l3', 'fig', generator_type, 'png', f'{generator_type}_comparison.png'))

    @staticmethod
    def generate_graphs_barabasi_albert(m_list, generator_type, N, width, height):

        fig = make_subplots(rows=len(m_list), cols=1,
                            subplot_titles=[f'Barabasi-Albert graph (N={N}, m={m})' for m in m_list])
        for i, m in enumerate(m_list):
            graph = generate_barabasi_albert(N, m)
            plot = PlotUtil.plot_graph_plotly(graph, f'Barabasi-Albert graph - List 3 (N={N}, m={m})')
            fig.add_trace(plot.to_dict()['data'][0], row=i + 1, col=1)
            fig.add_trace(plot.to_dict()['data'][1], row=i + 1, col=1)

        fig.update_layout(height=height,
                          width=width,
                          showlegend=False)
        fig.show()
        fig.write_html(os.path.join('l3', 'fig', generator_type, 'html', f'{generator_type}_comparison.html'))
        fig.write_image(os.path.join('l3', 'fig', generator_type, 'png', f'{generator_type}_comparison.png'))

    @staticmethod
    def generate_degree_histogram_random(df, height=4000, width=1000):
        fig = make_subplots(rows=df.shape[0], cols=1,
                            subplot_titles=[f'Node degree histogram for N = {int(N)}, p = {round(p, 2)}' for N, p in
                                            zip(df['N'], df['param'])])
        for index, row in df.iterrows():
            degrees, N, p = row['degrees_list'], row['N'], row['param']
            x = np.arange(binom.ppf(0.01, N, p),
                          binom.ppf(0.99, N, p))
            y = binom.pmf(x, N, p)

            fig.add_trace(go.Histogram(dict(x=degrees, histnorm='probability density', name='simulated degrees',
                                            legendgroup='simulated degrees', marker={'color': 'blue'},
                                            showlegend=True if index == 0 else False)),
                          row=index + 1, col=1)
            fig.add_trace(
                go.Scatter(dict(x=x, y=y), legendgroup='bernoulli distribution', name='bernoulli distribution',
                           marker={'color': 'red'}, showlegend=True if index == 0 else False),
                row=index + 1, col=1)
            fig['layout'][f'xaxis{index + 1}']['title'] = 'Nodes degree'
            fig['layout'][f'yaxis{index + 1}']['title'] = 'Probability density'
        fig.update_layout(height=height,
                          width=width)

        fig.write_html(os.path.join('l3', 'fig', 'random_graphs', 'html', f'node_degree_histogram.html'))
        fig.write_image(os.path.join('l3', 'fig', 'random_graphs', 'png', f'node_degree_histogram.png'))
        return fig

    @staticmethod
    def generate_degree_histogram_watts_strogatz(df, k, height=4000, width=1000):
        fig = make_subplots(rows=df.shape[0], cols=1,
                            subplot_titles=[f'Node degree histogram for N = {int(N)}, p = {round(p, 2)}, k = {k}' for
                                            N, p in zip(df['N'], df['param'])])
        for index, row in df.iterrows():
            degrees, N, p = row['degrees_list'], row['N'], row['param']
            fig.add_trace(go.Histogram(dict(x=degrees, histnorm='probability density', name='simulated degrees',
                                            legendgroup='simulated degrees', marker={'color': 'blue'},
                                            showlegend=True if index == 0 else False)),
                          row=index + 1, col=1)
            fig['layout'][f'xaxis{index + 1}']['title'] = 'Nodes degree'
            fig['layout'][f'yaxis{index + 1}']['title'] = 'Probability density'
        fig.update_layout(height=height,
                          width=width)
        fig.write_html(os.path.join('l3', 'fig', 'watts_strogatz', 'html', f'node_degree_histogram.html'))
        fig.write_image(os.path.join('l3', 'fig', 'watts_strogatz', 'png', f'node_degree_histogram.png'))
        return fig

    @staticmethod
    def generate_degree_histogram_barabasi_albert(df, height, width):
        fig = make_subplots(rows=df.shape[0], cols=1,
                            subplot_titles=[f'Node degree histogram for N = {int(N)}, m = {int(m)}' for N, m in
                                            zip(df['N'], df['param'])])
        for index, row in df.iterrows():
            degrees, N, p = row['degrees_list'], row['N'], row['param']
            fig.add_trace(go.Histogram(dict(x=degrees, histnorm='probability density', name='simulated degrees',
                                            legendgroup='simulated degrees', marker={'color': 'blue'},
                                            showlegend=True if index == 0 else False)),
                          row=index + 1, col=1)
            fig['layout'][f'xaxis{index + 1}']['title'] = 'Nodes degree'
            fig['layout'][f'yaxis{index + 1}']['title'] = 'Probability density'
        fig.update_layout(height=height,
                          width=width)
        fig.write_html(os.path.join('l3', 'fig', 'barabasi_albert', 'html', f'node_degree_histogram.html'))
        fig.write_image(os.path.join('l3', 'fig', 'barabasi_albert', 'png', f'node_degree_histogram.png'))
        return fig

    @staticmethod
    def generate_power_law_barabasi_albert(df_out, N=2000, height=4000, width=1000):
        fig = px.scatter(df_out, x='x', y='y', color='m', facet_row='m', height=height, width=width, trendline='ols',
                         trendline_color_override='red')
        fig.update_yaxes(matches=None)
        fig.update_xaxes(matches=None)
        for i in range(df_out.shape[0]):
            fig.update_layout(title=f'Barabasi-Albert power law for N = {N} and various m')
            fig.update_xaxes(title_text="Node degree (log10 scale)", row=df_out.shape[0] - i, col=1,
                             showticklabels=True)
            fig.update_yaxes(title_text="p (log10 scale)", row=df_out.shape[0] - i, col=1)
        fig.write_html(os.path.join('l3', 'fig', 'barabasi_albert', 'html', f'power_law.html'))
        fig.write_image(os.path.join('l3', 'fig', 'barabasi_albert', 'png', f'power_law.png'))
        fig.show()

    @staticmethod
    def visualize_random_walk(df_random_walk, agent, filename='random_walk', height=1000, width=1000,
                              frame_duration=300):
        fig = go.Figure(
            data=[go.Scatter(x=[0], y=[0])],
            layout=go.Layout(
                xaxis=dict(range=[agent.lattice.x_min - 1, agent.lattice.x_max + 1], autorange=False, title='x'),
                yaxis=dict(range=[agent.lattice.y_min - 1, agent.lattice.y_max + 1], autorange=False, title='y'),
                title="Random walk on 2-D Lattice",
                updatemenus=[dict(
                    type="buttons",
                    buttons=[dict(label="Play",
                                  method="animate",
                                  args=[None, {"frame": {"duration": frame_duration}}]),
                             dict(label="Stop",
                                  method="animate",
                                  args=[[None],
                                        dict(frame=dict(duration=0, redraw=False),
                                             mode='immediate')])])]
            ),
            frames=[go.Frame(data=[go.Scatter(x=df_random_walk.x[:i],
                                              y=df_random_walk.y[:i],
                                              mode='lines+markers',
                                              text=df_random_walk.time[:i],
                                              hovertemplate=
                                              "t=%{text}<br>" +
                                              "x=%{x}<br>" +
                                              "y=%{y}<br>",
                                              marker_color=df_random_walk.time[:i])],
                             layout=go.Layout(
                                 title_text=f'Random walk on 2-D Lattice for t = {df_random_walk.time[i]}'))
                    for i in range(len(df_random_walk))] + \
                   [go.Frame(data=[go.Scatter(x=df_random_walk.x,
                                              y=df_random_walk.y,
                                              text=df_random_walk.time,
                                              hovertemplate=
                                              "t=%{text}<br>" +
                                              "x=%{x}<br>" +
                                              "y=%{y}<br>",
                                              marker_color=df_random_walk.time)],
                             layout=go.Layout(title_text=f"Final state, t = {len(df_random_walk) - 1}"))]
        )
        fig.update_layout(width=width, height=height)
        fig.write_html(os.path.join('l4', 'fig', 'random_walk', 'html', f'{filename}.html'))
        return fig

    @staticmethod
    def generate_random_walk_histogram(vec, chart_title, x_axes_title, filename):
        fig = px.histogram(vec, histnorm='probability density')
        fig.update_xaxes(title_text=x_axes_title)
        fig.update_layout(title_text=chart_title)
        fig.write_html(os.path.join('l4', 'fig', f'{filename}.html'))
        return fig

    @staticmethod
    def generate_graph_random_walk_pngs(graph, visited_list, graph_type):
        for i, el in enumerate(visited_list):
            fig = PlotUtil.plot_graph_plotly(graph, f'Graph random walk: step = {i}, current node = {el}', color=el)
            fig.write_image(os.path.join('l4', 'fig', 'graph', graph_type, 'png', f'{i}.png'))

    @staticmethod
    def _get_edge_positions(graph: nx.classes.graph.Graph, positions: Dict[Any, float]):
        """ Creates x and y coordinates for edge plotly trace.

        Parameters:
            graph (Union[nx.Graph, Graph]): Graph representation in networkx or own implementation consistent with networkx framework.
            positions (Dict[Any, float]): x,y coordinates of nodes.

        Returns:
            edge_x (List[Union[float, NoneType]]): x coordinates of each edge for drawing the edges on a graph.
            edge_y (List[Union[float, NoneType]]): y coordinates of each edge for drawing the edges on a graph.
        """
        edge_x = []
        edge_y = []
        for edge in graph.edges():
            x0, y0 = positions[edge[0]]
            x1, y1 = positions[edge[1]]
            edge_x.append(x0)
            edge_x.append(x1)
            edge_x.append(None)
            edge_y.append(y0)
            edge_y.append(y1)
            edge_y.append(None)

        return edge_x, edge_y

    @staticmethod
    def _get_node_positions(graph: nx.classes.graph.Graph, positions: Dict[Any, float]):
        """ Creates x and y coordinates for node plotly trace.

        Parameters:
            graph (Union[nx.Graph, Graph]): Graph representation in networkx or own implementation consistent with networkx framework.
            positions (Dict[Any, float]): x,y coordinates of nodes.

        Returns:
            edge_x (List[float]): x coordinates of each node for drawing the edges on a graph.
            edge_y (List[float]): y coordinates of each node for drawing the edges on a graph.
        """
        node_x = []
        node_y = []
        for node in graph.nodes():
            x, y = positions[node]
            node_x.append(x)
            node_y.append(y)

        return node_x, node_y

    @staticmethod
    def _get_node_adjacencies(graph: nx.classes.graph.Graph):
        """ Creates lists with number of node neighbours and texts, which are used for creating plotly traces.

        Parameters:
            graph (Union[nx.Graph, Graph]): Graph representation in networkx or own implementation consistent with networkx framework.
        Returns:
            node_adjacencies (List[int]): x coordinates of each node for drawing the edges on a graph.
            node_text (List[str]): y coordinates of each node for drawing the edges on a graph.
        """
        node_adjacencies = []
        node_text = []
        for node, adjacencies in enumerate(graph.adjacency()):
            node_adjacencies.append(len(adjacencies[1]))
            node_text.append('# of connections: ' + str(len(adjacencies[1])))

        return node_adjacencies, node_text
