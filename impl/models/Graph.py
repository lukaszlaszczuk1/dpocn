import logging
import numpy as np
from heapq import heappush, heappop

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logging.debug("Start logging")


class Graph:
    def __init__(self, name=None):
        self._nodes = list()
        self._edges = list()
        self.name = name
        self.adjacency_matrix = None

    def add_node(self, name, label=None, color=None):
        if not self.__contains__(name):
            self._nodes.append(self.Node(name, label, color))
        return

    def add_nodes_from_list(self, nodes_name_list, nodes_label_list=None, nodes_color_list=None):
        nodes_label_list = len(nodes_name_list) * [None] if not nodes_label_list else nodes_label_list
        nodes_color_list = len(nodes_name_list) * [None] if not nodes_color_list else nodes_color_list
        for name, label, color in zip(nodes_name_list, nodes_label_list, nodes_color_list):
            self.add_node(name)
        return

    def add_nodes_from_list_fast(self, nodes_name_list):
        for name in nodes_name_list:
            self._nodes.append(self.Node(name, None, None))
        return

    def add_edge(self, node_1, node_2, weight=None, add_if_exists=True):
        if self.__contains__(node_1) and self.__contains__(node_2):
            is_edge = self.contains_edge(node_1, node_2)
            if is_edge and not add_if_exists:
                return
            elif is_edge and add_if_exists:
                edge = self.contains_edge(node_1, node_2, return_edge=True)
                self._edges.remove(edge)
                self._edges.append(self.Edge(node_1, node_2, weight))
                return
            else:
                self._edges.append(self.Edge(node_1, node_2, weight))
                return
        else:
            self.add_node(node_1)
            self.add_node(node_2)
            self._edges.append(self.Edge(node_1, node_2, weight))
            return

    def add_edges_from_list(self, edge_list, add_if_exists=True):
        for edge in edge_list:
            if len(edge) == 2:
                self.add_edge(edge[0], edge[1], None, add_if_exists)
            else:
                self.add_edge(edge[0], edge[1], edge[2], add_if_exists)
        return

    def add_edges_from_list_fast(self, edge_list):
        for nodes in edge_list:
            self._edges.append(self.Edge(nodes[0], nodes[1], None))
        return

    def edges(self):
        return [(edge.node_1, edge.node_2, edge.weight) for edge in self._edges]

    def nodes(self):
        return [node.name for node in self._nodes]

    def get_neighbours(self, node_name):
        out_list = list()
        for edge in self._edges:
            if edge.node_1 == node_name:
                out_list.append(edge.node_2)
            elif edge.node_2 == node_name:
                out_list.append(edge.node_1)
        return out_list

    def get_degrees(self):
        return np.sum(self.adjacency_matrix, axis=0)

    def adjacency(self):
        return [(node_name, self.get_neighbours(node_name)) for node_name in self.nodes()]

    def __contains__(self, new_node_name):
        for node in self._nodes:
            if node.name == new_node_name:
                return True
        return False

    def contains_edge(self, node_1, node_2, return_edge=False):
        for edge in self._edges:
            edge_set = set([edge.node_1, edge.node_2])
            if set([node_1, node_2]) == edge_set:
                if return_edge:
                    return edge
                else:
                    logger.debug(f"Edge {(edge.node_1, edge.node_2)} already exists!")
                    return True
        logger.debug(f"Edge {(node_1, node_2)} does not exist!")
        return False

    def get_shortest_paths(self, node_name):
        result = dict()
        h = []
        for node in self._nodes:
            if node.name != node_name:
                heappush(h, (np.inf, node.name))
            else:
                heappush(h, (0, node.name))
        visited_nodes = []
        while len(h) != 0:
            current_node = heappop(h)
            for neighbour in self.get_neighbours(current_node[1]):
                if neighbour not in visited_nodes:
                    edge_weight = self.contains_edge(neighbour, current_node[1], return_edge=True).weight
                    edge_weight = 1 if not edge_weight else edge_weight
                    heappush(h, (current_node[0] + edge_weight, neighbour))
            if current_node[1] not in visited_nodes:
                visited_nodes.append(current_node[1])
                result[current_node[1]] = current_node[0]
        return result

    def save_graph(self, filename=None, layout="dot"):
        name = self.name.replace(' ', '') if self.name else "G"
        out = f"graph {name}\u007b\n\tlayout = {layout}\n"
        for edge in self.edges():
            if edge[2]:
                out += f'\t{edge[0]} -- {edge[1]} [label="{edge[2]}"];\n'
            else:
                out += f'\t{edge[0]} -- {edge[1]};\n'
        out += '\u007d'
        if filename:
            with open(filename, 'w') as f:
                f.writelines(out)
        else:
            return out

    class Node:
        def __init__(self, name, label=None, color=None):
            self.name = name
            self.label = label
            self.color = color
            setattr(self, str(self.name), str(self.name))

        def __repr__(self):
            return f"Node({self.name})"

        def __getitem__(self, item):
            return getattr(self, item)

    class Edge:
        def __init__(self, node_1, node_2, weight=None):
            self.node_1 = node_1
            self.node_2 = node_2
            self.weight = weight
            self.attributes = [self.node_1, self.node_2, self.weight]

        def __repr__(self):
            return f"Edge{(self.node_1, self.node_2, self.weight)}"

        def __getitem__(self, item):
            return self.attributes[item]
