import numpy as np
from impl.models.Graph import Graph
import time


def generate_random_graph(N, p):
    graph = Graph()
    graph.add_nodes_from_list_fast(np.arange(N))
    random_adjacency, random_adjacency_mat = get_random_adjacency(N, p)
    graph.adjacency_matrix = random_adjacency_mat
    graph.add_edges_from_list_fast(random_adjacency)
    return graph


def get_random_adjacency(N, p):
    mat = np.triu(np.random.random((N, N)))
    np.fill_diagonal(mat, 0)
    bool_mat = mat > 1 - p
    random_edges = np.where(bool_mat)
    result = zip(random_edges[0], random_edges[1])
    adj_mat = bool_mat + bool_mat.T
    return result, adj_mat


def generate_watts_strogatz(N, p, k=4):
    k = k if k % 2 == 0 else k + 1
    graph = Graph()
    graph.add_nodes_from_list_fast(np.arange(N))
    mat_init = get_initial_watts_strogatz_adjacency(N, k)
    adjacency_list, adjacency_matrix = get_watts_strogatz_final_adjacency(mat_init, N, p)
    graph.adjacency_matrix = adjacency_matrix
    graph.add_edges_from_list_fast(adjacency_list)
    return graph


def get_initial_watts_strogatz_adjacency(N, k):
    init_mat = np.zeros((N, N), dtype=bool)
    k_list = list(range(-(k // 2), 0)) + list(range(1, k // 2 + 1))
    for i in range(N):
        init_mat[[k * [i]], [(i + el) % N for el in k_list]] = True
    init_mat = np.triu(init_mat)
    return init_mat


def get_watts_strogatz_final_adjacency(init_mat, N, p):
    edges = np.where(init_mat)
    for i, j in zip(edges[0], edges[1]):
        if np.random.random() < p:
            new_value = np.random.choice(np.arange(N))
            if not init_mat[i, new_value] and not init_mat[new_value, i]:
                while i == new_value:
                    new_value = np.random.choice(np.arange(N))
                subset_index_1, subset_index_2 = (i, new_value) if i < new_value else (new_value, i)
                init_mat[subset_index_1, subset_index_2] = True
                init_mat[i, j] = False
    edges = np.where(init_mat)
    adjacency_list = zip(edges[0], edges[1])
    final_adjacency_matrix = init_mat + init_mat.T
    return adjacency_list, final_adjacency_matrix


def generate_barabasi_albert(N, m):
    graph = Graph()
    graph.add_nodes_from_list_fast(np.arange(N))
    nodes = np.arange(N)
    adj_mat = np.zeros((N, N), dtype=bool)
    adj_mat[list(range(m)), [m] * m] = True
    adj_mat[[m] * m, list(range(m))] = True
    sum_ = 2 * m
    for i in range(m + 1, N):
        prob_array = np.sum(adj_mat, axis=1) / sum_
        idx = np.random.choice(nodes, m, replace=False, p=prob_array)
        adj_mat[idx, [i] * m] = True
        adj_mat[[i] * m, idx] = True
        sum_ += 2 * m
    adj_mat = np.triu(adj_mat)
    edges = np.where(adj_mat)
    adjacency_list = zip(edges[0], edges[1])
    graph.adjacency_matrix = adj_mat + adj_mat.T
    graph.add_edges_from_list_fast(adjacency_list)
    return graph
