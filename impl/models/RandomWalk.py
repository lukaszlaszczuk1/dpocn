import pandas as pd
import numpy as np

DEFAULT_BOUNDARY = 10


class Lattice:
    def __init__(self, boundaries_dict=dict()):
        self.x_min = boundaries_dict.get('x_min', -DEFAULT_BOUNDARY)
        self.x_max = boundaries_dict.get('x_max', DEFAULT_BOUNDARY)
        self.y_min = boundaries_dict.get('y_min', -DEFAULT_BOUNDARY)
        self.y_max = boundaries_dict.get('y_max', DEFAULT_BOUNDARY)


class Agent:
    def __init__(self, lattice=Lattice(), max_steps=50, x_start=0, y_start=0):
        self.lattice = lattice
        self.steps_left = max_steps
        self.x_pos = x_start
        self.y_pos = y_start
        self.position_history = [(x_start, y_start)]

    def simulate_random_walk(self, max_steps=50, type_='simple'):
        self.reset(max_steps)
        simple = True if type_ == 'simple' else False
        while self.steps_left > 0:
            self.x_pos, self.y_pos = self.update_positions(simple=simple)
            self.position_history.append((self.x_pos, self.y_pos))
            self.steps_left -= 1
        return self.position_history_to_df()

    def update_positions(self, simple=False):
        random_angle = Agent.simulate_random_angle(simple)
        new_x = self.x_pos + np.cos(random_angle)
        new_y = self.y_pos + np.sin(random_angle)
        while self.lattice.x_min > new_x or self.lattice.x_max < new_x \
                or self.lattice.y_min > new_y or self.lattice.y_max < new_y:
            random_angle = self.simulate_random_angle(simple)
            new_x = self.x_pos + np.cos(random_angle)
            new_y = self.y_pos + np.sin(random_angle)
        return new_x, new_y

    def position_history_to_df(self):
        return pd.DataFrame(self.position_history).reset_index().rename(columns={'index': 'time', 0: 'x', 1: 'y'})

    def reset(self, max_steps):
        self.steps_left = max_steps
        self.x_pos = self.position_history[0][0]
        self.y_pos = self.position_history[0][1]
        self.position_history = [(self.x_pos, self.y_pos)]

    @staticmethod
    def simulate_random_angle(simple=False):
        return np.random.choice([0, np.pi / 2, np.pi, -np.pi / 2]) if simple else np.random.uniform(0, 2 * np.pi)


def graph_random_walk(graph, n_steps, starting_node):
    visited_nodes = [starting_node]
    current_node = starting_node
    for i in range(n_steps):
        current_node = np.random.choice(graph.get_neighbours(current_node))
        visited_nodes.append(current_node)
    return visited_nodes
